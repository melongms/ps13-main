﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BuyableFactory : MonoBehaviour {

    public class NotEnoughMoneyException : System.Exception { }
    public class GameObjectIsNotPlayerException : System.Exception { }
    public class CostMustBePositiveException : System.Exception { }

    public class MustBeLessThanMaxException : System.Exception {
        public MustBeLessThanMaxException(string message) : base(message) {}
    }


    public abstract class BuyableItem
    {
        public const long MAX_CHARACTER_LENGTH_FOR_DESCRIPTION = 1000;

        private int cost;
        public int Cost {
            get { return cost; }
            set
            {
                if(value >= 0)
                {
                    cost = value;
                }
                else
                {
                    throw new CostMustBePositiveException();
                }
            }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                if (value.Length <= MAX_CHARACTER_LENGTH_FOR_DESCRIPTION)
                {
                    description = value;
                }
                else
                {
                    throw new MustBeLessThanMaxException("Max is: " + MAX_CHARACTER_LENGTH_FOR_DESCRIPTION);
                }
            }
        }
        
        public abstract BuyableItem Buy(GameObject player);
    }

    private static class MethodStorage
    {
        public static BuyableItem Buy(GameObject player, int cost, BuyableItem instance)
        {
            if (player.CompareTag("Player"))
            {
                if (player.GetComponent<Inventory>().Money >= cost)
                {
                    player.GetComponent<Inventory>().Money -= cost;
                    return instance;
                }
                else
                {
                    throw new NotEnoughMoneyException();
                }
            }
            else
            {
                throw new GameObjectIsNotPlayerException();
            }
        }
    }

    public class CryoBot : BuyableItem
    {
        public const int MAX_DEPTH = 30000;

        private short maxDepth = 3000;
        public short MaxDepth
        {
            get { return maxDepth; }
            set
            {
                if (value <= MAX_DEPTH)
                {
                    maxDepth = value;
                }
                else
                {
                    throw new MustBeLessThanMaxException("Max is: " + MAX_DEPTH);
                }
            }
        }

        public override BuyableItem Buy(GameObject player)
        {
            return MethodStorage.Buy(player, Cost, new CryoBot());
        }

        public static implicit operator CryoBot(GameObject v)
        {
            throw new NotImplementedException();
        }
    }

    public class HydroBot : BuyableItem
    {
        public const byte MAX_ITEMS = 30;

        private byte maxItems = 10;
        public byte MaxItems
        {
            get { return maxItems; }
            set
            {
                if (value <= MAX_ITEMS)
                {
                    maxItems = value;
                } else
                {
                    throw new MustBeLessThanMaxException("Max is: " + MAX_ITEMS);
                }
            }
        }

        public override BuyableItem Buy(GameObject player)
        {
            return MethodStorage.Buy(player, Cost, new HydroBot());
        }
    }
}
