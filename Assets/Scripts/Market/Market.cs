﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Market {

    public class NotEnoughMoneyException : System.Exception { }
    public class GameObjectIsNotPlayerException : System.Exception { }
    public class CostMustBePositiveException : System.Exception { }

    public interface IHasCost
    {
        int GetCost();
    }

    public interface IKryobot
    {
        int GetMaxDepth();
        int GetCurrentDepth();
    }

    public interface IHydrobot
    {
        GameObject[] GetItems();
    }

}
