﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketButtonHandler : MonoBehaviour {

    public GameObject ForSell;

    bool playCancel = false;
    float timer = 1f;

    Color initialColor;

    void Start()
    {
        initialColor = GetComponent<MeshRenderer>().material.color;
        ForSell.transform.position = transform.position;
    }

    void Buy()
    {
        try
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<Inventory>().Money -= ForSell.GetComponent<Bot>().GetCost();
            if (ForSell.CompareTag("KryobotMarketTag"))
            {
                player.GetComponent<Inventory>().kryobot = Instantiate(ForSell);
            }
            else if (ForSell.CompareTag("HydrobotMarketTag"))
            {
                player.GetComponent<Inventory>().hydrobot = Instantiate(ForSell);
            }
        } catch (BuyableFactory.NotEnoughMoneyException)
        {
            playCancel = true;        
        }
        
    }

    void Update()
    {

        ForSell.transform.Rotate(new Vector3(0, 1, 0));

        if (playCancel)
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                playCancel = false;
                GetComponent<MeshRenderer>().material.color = initialColor;
            }
        }
    }
}
