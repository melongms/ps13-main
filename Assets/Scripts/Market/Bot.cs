﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour, Market.IHasCost, Market.IHydrobot, Market.IKryobot {

    public class BotIsNotKryobotException : System.Exception { }
    public class BotIsNotHydrobotException : System.Exception { }

    public string Name;
    public int CurrentDepth;
    public int MaxDepth;
    public GameObject[] Items;
    public bool IsKryobot = true;

    private int cost;
    public int Cost {
        get
        {
            return cost;
        }
        set {
            if (value >= 0)
            {
                cost = value;
            } else
            {
                throw new Market.CostMustBePositiveException();
            }
        }
    }
    
    public int GetCost()
    {
        return Cost;
    }

    public int GetCurrentDepth()
    {
        if (IsKryobot)
        {
            return CurrentDepth;
        } else
        {
            throw new BotIsNotKryobotException();
        }
    }

    public int GetMaxDepth()
    {
        if (IsKryobot)
        {
            return MaxDepth;
        } else
        {
            throw new BotIsNotKryobotException();
        }
    }

    public GameObject[] GetItems()
    {
        if(!IsKryobot)
        {
            return Items;
        } else
        {
            throw new BotIsNotHydrobotException();
        }
    }
}
