﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KryobotButtonHandler : MonoBehaviour
{

    bool isEntered = false;
  
    public void OnPlayerRaycastEnter()
    {
        if (!isEntered)
        {
            GetComponent<MeshRenderer>().material.color = Color.black;
            transform.parent.GetChild(1)
                .GetChild(0).GetComponent<MeshRenderer>().material.SetFloat("_EmissionColor", 0.5f);
            transform.parent.GetChild(1)
                .GetChild(0).GetComponent<MeshRenderer>().material.color = Color.white;
            transform.parent.GetChild(1)
                .GetChild(1).GetComponent<MeshRenderer>().material.SetFloat("_EmissionColor", 0.5f);
            transform.parent.GetChild(1)
                .GetChild(1).GetComponent<MeshRenderer>().material.color = Color.white;
            isEntered = true;
        }
        if (Input.GetMouseButtonDown(0))
        {
            GetComponent<MeshRenderer>().material.color = Color.green;
            //переход
        }
    }

    public void OnPlayerRaycastExit()
    {
        if (isEntered)
        {
            GetComponent<MeshRenderer>().material.color = Color.white;
            transform.parent.GetChild(1)
                .GetChild(0).GetComponent<MeshRenderer>().material.SetFloat("_EmissionColor", 0.0f);
            transform.parent.GetChild(1)
                .GetChild(0).GetComponent<MeshRenderer>().material.color = Color.black;
            transform.parent.GetChild(1)
                .GetChild(1).GetComponent<MeshRenderer>().material.SetFloat("_EmissionColor", 0.0f);
            transform.parent.GetChild(1)
                .GetChild(1).GetComponent<MeshRenderer>().material.color = Color.black;
            isEntered = false;
        }
    }


}
