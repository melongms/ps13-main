﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

	public GameObject kryobot;
    public GameObject hydrobot;

    private int money;

    public int Money
    {
        get
        {
            return money;
        }

        set
        {
            if (value >= 0)
            {
                money = value;
            }
            else
            {
                throw new Market.NotEnoughMoneyException();
            }
        }

    }

}
