﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastManagement : MonoBehaviour
{
    public bool DEBUG = false;
    public int LengthOfHand = 4;

    void Update()
    {

        RaycastHit hit;
        Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

        //Ray ray = Camera.main.ScreenPointToRay(new Vector2(0f, 0f));
        if (DEBUG)
        {
            Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * LengthOfHand);
            //Debug.DrawRay(ray.origin, ray.direction);
        }

        if (Physics.Raycast(ray, out hit, LengthOfHand))
        {
            switch (hit.collider.tag)
            {
                case "Kryobot Station":
                    //Debug.Log("Found kryobot station!");
                    if (Physics.Raycast(ray, out hit, LengthOfHand, 1 << LayerMask.NameToLayer("KryobotIcons")))
                    {
                        switch (hit.collider.tag)
                        {
                            case "EjectResourcesButton":
                                hit.collider.gameObject.GetComponent<KryobotButtonHandler>().OnPlayerRaycastEnter();
                                break;
                            case "ToKryobotButton":
                                hit.collider.gameObject.GetComponent<KryobotButtonHandler>().OnPlayerRaycastEnter();
                                break;
                            case "ToHydrobotButton":
                                hit.collider.gameObject.GetComponent<KryobotButtonHandler>().OnPlayerRaycastEnter();
                                break;
                        }

                    }
                    else
                    {
                        foreach (GameObject o in GameObject.FindGameObjectsWithTag("Kryobot Station"))
                        {
                            foreach (KryobotButtonHandler sc in o.GetComponentsInChildren<KryobotButtonHandler>())
                            {
                                sc.OnPlayerRaycastExit();
                            }
                        }
                    }
                    break;
                case "Sell Button":
                    //Debug.Log("Found sell button!");
                    break;
                case "Market Button":
                    //Debug.Log("Found Market button!");
                    break;
                case "Resource Sphere":
                    if (Input.GetMouseButton(0))
                    {
                        hit.collider.gameObject.GetComponent<ResourceSphereScript>().switchFollowing();
                    }
                   // Debug.Log("Found resource sphere!");
                    break;
                case "Storage":
                    //Debug.Log("Found storage!");
                    break;
            }

        }
    }

    void StopDragging()
    {

    }
}
