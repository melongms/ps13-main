﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceSphereScript : MonoBehaviour {

    public GameObject ResourceIn;
    public bool Attached = false;
    public float ScaleFactor = 0.5f;
    public bool Following = false;
    public float SpeedOfFollowing = 0.05f;

    private bool flying = true;

    void Start()
    {
        if (ResourceIn != null)
        {
            ResourceIn.transform.position = transform.position;
            ResourceIn.transform.parent = transform;
            ResourceIn.transform.localScale = new Vector3(ResourceIn.transform.localScale.x * ScaleFactor,
                ResourceIn.transform.localScale.y * ScaleFactor, ResourceIn.transform.localScale.z * ScaleFactor);
        }
    }

    void Update()
    {
        if (Following)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            if (flying)
            {
                transform.position = Vector3.Lerp(transform.position, Camera.main.transform.position, SpeedOfFollowing);
            }   
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (Following && collision.collider.tag == "Player")
        {
            flying = false;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (Following && collision.collider.tag == "Player")
        {
            flying = true;
        }
    }

    public void switchFollowing()
    {
        Following = !Following;
        flying = true;
        GetComponent<Rigidbody>().useGravity = !Following;
    }

    public void InsertIntoResourceSphere(GameObject gameObject)
    {
        ResourceIn = gameObject;
    }

}
